//
//  Picker.swift
//  demo_app
//
//  Created by rogiso on 2020/04/03.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit
class Picker:UIViewController{
    
    @IBOutlet weak var ColorBar_R: UISlider!
    @IBOutlet weak var ColorBar_G: UISlider!
    @IBOutlet weak var ColorBar_B: UISlider!
    @IBOutlet weak var Color_View: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        Color_View.frame = CGRect(x: 10, y: 60, width: UIScreen.main.bounds.size.width-20, height: 50)
        Color_View.backgroundColor = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.0)
       self.view.addSubview(Color_View)
        Color_View.layer.borderWidth = 1.0
        Color_View.layer.borderColor = UIColor(red: 0.82, green: 0.82, blue: 0.82, alpha: 1.0).cgColor
        
     //   ColorBar_R.frame = CGRect(x: 32, y: 140, width: UIScreen.main.bounds.size.width-52, height: 10)
        ColorBar_R.minimumValue = 0.0
        ColorBar_R.maximumValue = 1.0
        ColorBar_R.value = 0.0
        ColorBar_R.addTarget(self, action: #selector(changeColorSlider), for: UIControl.Event.valueChanged)
        self.view.addSubview(ColorBar_R)
        ColorBar_R.tag = 1
        
      //  ColorBar_G.frame = CGRect(x: 32, y: 180, width: UIScreen.main.bounds.size.width-52, height: 10)
        ColorBar_G.minimumValue = 0.0
        ColorBar_G.maximumValue = 1.0
        ColorBar_G.value = 0.0
        ColorBar_G.addTarget(self, action: #selector(changeColorSlider), for: UIControl.Event.valueChanged)
        self.view.addSubview(ColorBar_G)
        ColorBar_G.tag = 2
        
      //  ColorBar_B.frame = CGRect(x: 32, y: 220, width: UIScreen.main.bounds.size.width-52, height: 10)
        ColorBar_B.minimumValue = 0.0
        ColorBar_B.maximumValue = 1.0
        ColorBar_B.value = 0.0
        ColorBar_B.addTarget(self, action: #selector(changeColorSlider), for: UIControl.Event.valueChanged)
        self.view.addSubview(ColorBar_G)
        ColorBar_B.tag = 2
    }
    @objc func changeColorSlider(_ sender: UISlider) {
        switch (sender.tag) {
        case 1:
            print("赤のスライダーの値: \(sender.value)")
        case 2:
            print("緑のスライダーの値: \(sender.value)")
        case 3:
            print("青のスライダーの値: \(sender.value)")
        default:
            break
        }
        Color_View.backgroundColor = UIColor(
            red: CGFloat(ColorBar_R.value),
            green: CGFloat(ColorBar_G.value),
            blue: CGFloat(ColorBar_B.value),
            alpha: 1.0)
    }
}
