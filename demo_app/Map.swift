//
//  Map.swift
//  demo_app
//
//  Created by rogiso on 2020/04/03.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit
//MapKitのインポート
import MapKit

class Map: UIViewController{
    
    @IBOutlet weak var myMapview: MKMapView!
    override func viewDidLoad() {
           super.viewDidLoad()
        // 東京駅の位置情報を設定（緯度: 35.681236 経度: 139.767125）
               let latitude = 35.681236
               let longitude = 139.767125
               // 緯度・軽度を設定
               let location = CLLocationCoordinate2DMake(latitude, longitude)
           //MapViewを生成し、表示する
           //let myMapView = MKMapView()
        
            myMapview.frame = self.view.frame
            myMapview.setCenter(location, animated: true)
        
        // 縮尺を設定
        var region = myMapview.region
        region.center = location
        region.span.latitudeDelta = 0.02
        region.span.longitudeDelta = 0.02
        // マップビューに縮尺を設定
        myMapview.setRegion(region, animated:true)
        
        //ピンの生成
        let pin = MKPointAnnotation()
        //ピンを置く場所を指定
        pin.coordinate = location
        pin.title = "Tokyo.St"
        
        myMapview.addAnnotation(pin)
        self.view.addSubview(myMapview)
        
       }
    
}
