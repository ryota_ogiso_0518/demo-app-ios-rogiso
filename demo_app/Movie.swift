//
//  Movie.swift
//  demo_app
//
//  Created by rogiso on 2020/04/03.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit

class Movie:UIViewController{
    override func viewDidLoad() {
           super.viewDidLoad()
           setVideoView()
           // Do any additional setup after loading the view.
       }
       
       func setVideoView(){
           //動画ファイルのURLを取得
        let path = Bundle.main.path(forResource: "video01", ofType: "mp4")
            let url = URL.init(fileURLWithPath: path!)
           let player:AVPlayer = AVPlayer.init(url: url)
        let controller:AVPlayerViewController = AVPlayerViewController.init()
           controller.player = player
           controller.view.frame = CGRect(x:0, y:0, width:self.view.bounds.size.width,height:self.view.bounds.size.height)
           self.addChild(controller)
           self.view.addSubview(controller.view)
           player.play()
       }
}
