//
//  Form.swift
//  demo_app
//
//  Created by rogiso on 2020/04/06.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit

let checkedImage : UIImage = UIImage(named: "Image-4")!
let uncheckedImage : UIImage = UIImage(named: "Image-3")!

class Form: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
   
    
    
    @IBOutlet weak var seibetsu: UISegmentedControl!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var pickerView_M: UIPickerView!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var pickerView_D: UIPickerView!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var checkBox2: UIButton!
    @IBOutlet weak var checkBox3: UIButton!
    @IBOutlet weak var checkBox4: UIButton!
    @IBOutlet weak var checkBox5: UIButton!
    
    
    var flag = false
    var flag2 = false
    var flag3 = false
    var flag4 = false
    var flag5 = false
    
    let list: [Int] = ([Int])(1950...2020)
        
    let list2: [Int] = ([Int])(1...12)
    
    let list3: [Int] = ([Int])(1...31)
    
    var a = 0
    var b = 0
    var c = 0
    var Y = 2020
    var age = 0
    
    var dt = Date()
    let dateformat = DateFormatter()
    let dateformat_m = DateFormatter()
    let dateformat_d = DateFormatter()
    
    var x : [String] = [""]
    var y : [String] = [""]
    var m : [String] = [""]
    var d : [String] = [""]
    
   
    let calendar = Calendar(identifier: .gregorian)
    
    
    
  @IBAction func checkBox(_ sender: UIButton) {
    print("click")
                 if flag == false {
                    
                     self.checkBox.setImage(checkedImage, for: .normal)
                     
                     flag = true
                 }else{
                     self.checkBox.setImage(uncheckedImage, for: .normal)
                     flag = false
                 }
             }
    
    @IBAction func checkBox2(_ sender: UIButton) {
    print("click")
                 if flag2 == false {
                    
                     self.checkBox2.setImage(checkedImage, for: .normal)
                     
                     flag2 = true
                 }else{
                     self.checkBox2.setImage(uncheckedImage, for: .normal)
                     flag2 = false
                 }
             }
    
    @IBAction func checkBox3(_ sender: UIButton) {
       print("click")
                    if flag3 == false {
                       
                        self.checkBox3.setImage(checkedImage, for: .normal)
                        
                        flag3 = true
                    }else{
                        self.checkBox3.setImage(uncheckedImage, for: .normal)
                        flag3 = false
                    }
                }
    
    @IBAction func checkBox4(_ sender: UIButton) {
       print("click")
                    if flag4 == false {
                       
                        self.checkBox4.setImage(checkedImage, for: .normal)
                        
                        flag4 = true
                    }else{
                        self.checkBox4.setImage(uncheckedImage, for: .normal)
                        flag4 = false
                    }
                }
    
    @IBAction func checkBox5(_ sender: UIButton) {
       print("click")
                    if flag5 == false {
                       
                        self.checkBox5.setImage(checkedImage, for: .normal)
                        
                        flag5 = true
                    }else{
                        self.checkBox5.setImage(uncheckedImage, for: .normal)
                        flag5 = false
                    }
                }
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            pickerView.delegate = self
            pickerView.dataSource = self
            pickerView.tag = 0
            pickerView_M.delegate = self
            pickerView_M.dataSource = self
            pickerView_M.tag = 1
            pickerView_D.delegate = self
            pickerView_D.dataSource = self
            pickerView_D.tag = 2
            
           
            
          
            
            
          
            
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
            let toolbar2 = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
            let toolbar3 = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
            let spacelItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
            let doneItem2 = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
            let doneItem3 = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
            toolbar.setItems([spacelItem, doneItem], animated: true)
            toolbar2.setItems([spacelItem, doneItem2], animated: true)
            toolbar3.setItems([spacelItem, doneItem3], animated: true)
            textField.inputView = pickerView
            textField.inputAccessoryView = toolbar
            
            textField2.inputView = pickerView_M
            textField2.inputAccessoryView = toolbar2
            
            textField3.inputView = pickerView_D
            textField3.inputAccessoryView = toolbar3
        }
    @objc func done() {
        textField.endEditing(true)
         textField2.endEditing(true)
         textField3.endEditing(true)
        textField.text = "\(list[pickerView.selectedRow(inComponent: 0)])"
        textField2.text = "\(list2[pickerView_M.selectedRow(inComponent: 0)])"
         textField3.text = "\(list3[pickerView_D.selectedRow(inComponent: 0)])"
    }
    func numberOfComponents(in pickerView : UIPickerView) -> Int {
        return 1
    }
    // UIPickerViewの行数、リストの数
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           if pickerView.tag == 0 {     // <<<<<<<<<<　変更
               return list.count
           } else if pickerView.tag == 1 {
               return list2.count
           }else {
             return list3.count
        }
       }
    // UIPickerViewの最初の表示
   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {    // <<<<<<<<<<　変更
            return String (list[row])
        } else if pickerView.tag == 1 {
            return String (list2[row])
        }else{
            return String (list3[row])
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {      // <<<<<<<<<<　変更
            textField.text = String (list[row])
            a = (list[row])
        } else if pickerView.tag == 1 {
            textField2.text = String (list2[row])
            b = (list2[row])
        }else{
            textField3.text = String (list3[row])
             c = (list3[row])
        }
        if(a != 0 && b != 0 && c != 0){
            age = 2020 - a
                                
            if(b>4){
            age -= 1
            }else if(b == 4 && c > 8){
            age -= 1
            }
        }
                   
            ageText.text = String(age)
      
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "form2"{
            let name =  segue.destination as! Form2
            name.Name = nameField.text!
            let year = segue.destination as! Form2
            year.year = a
            let month = segue.destination as! Form2
            month.month = b
             let day = segue.destination as! Form2
            day.day = c
             let Age = segue.destination as! Form2
            Age.Age = age
             let Flag = segue.destination as! Form2
            Flag.chekc1 = flag
            let Flag2 = segue.destination as! Form2
            Flag2.chekc2 = flag2
            let Flag3 = segue.destination as! Form2
            Flag3.chekc3 = flag3
            let Flag4 = segue.destination as! Form2
            Flag4.chekc4 = flag4
            let Flag5 = segue.destination as! Form2
            Flag5.chekc5 = flag5
            
            let seibetu = segue.destination as! Form2
            seibetu.seibetu = seibetsu.selectedSegmentIndex
        }
        
    }
    
}
     
