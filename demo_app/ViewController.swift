//
//  ViewController.swift
//  demo_app
//
//  Created by rogiso on 2020/04/01.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var TableView: UITableView!
    
    let list = ["Picker", "Map", "WebView", "PlayVideo", "View Pager","Form"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         //self.performSegue(withIdentifier: "move_picker", sender: nil)
        return list.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     // セルを取得する
        let cell_picker: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Picker", for: indexPath)
        // セルに表示する値を設定する
        cell_picker.textLabel!.text = list[indexPath.row]
//        cell_map.textLabel!.text = list[1]
//        cell_video.textLabel!.text = list[2]
//        cell_web.textLabel!.text = list[3]
//        cell_pager.textLabel!.text = list[4]
//        cell_form.textLabel!.text = list[5]
       
        
        return cell_picker
    }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row==0{
          // セルの選択を解除
          tableView.deselectRow(at: indexPath, animated: true)
   
          // 別の画面に遷移
            performSegue(withIdentifier: "showPicker", sender: nil)
    }else if indexPath.row==1{
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                 performSegue(withIdentifier: "showMap", sender: nil)
        }else if indexPath.row==2{
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                 performSegue(withIdentifier: "showWeb", sender: nil)
        }else if indexPath.row==3{
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                 performSegue(withIdentifier: "showVideo", sender: nil)
        }else if indexPath.row==4{
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                 performSegue(withIdentifier: "showPager", sender: nil)
        }else if indexPath.row==5{
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                 performSegue(withIdentifier: "showForm", sender: nil)
        }
    }

}

