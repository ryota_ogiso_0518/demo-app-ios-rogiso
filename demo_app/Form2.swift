//
//  Form2.swift
//  demo_app
//
//  Created by rogiso on 2020/04/09.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit

class Form2 : UIViewController{
    var Name = ""
    var seibetu = 0
    var year = 0
    var month = 0
    var day = 0
    var Age = 0
    var chekc1 = false
    var chekc2 = false
    var chekc3 = false
    var chekc4 = false
    var chekc5 = false
    
    var text_ = ""
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var seibeitsu: UILabel!
    @IBOutlet weak var Year: UILabel!
    @IBOutlet weak var Month: UILabel!
    @IBOutlet weak var Day: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var text: UILabel!
    
    
    override func viewDidLoad() {
        let age_text = String(Age)
        let year_text = String(year)
        let month_text = String(month)
        let day_text = String(day)
        
        name.text = Name
        age.text = age_text
        Year.text = year_text
        Month.text = month_text
        Day.text = day_text
        
        if(seibetu == 0){
            seibeitsu.text = "男性"
        }else if(seibetu == 1){
            seibeitsu.text = "女性"
        }else{
            seibeitsu.text = "その他"
        }
        
        if(chekc1 == true){
            text_ += "インターネット "
        }
        if(chekc2 == true){
            text_ += "雑誌記事　"
        }
        if(chekc3 == true){
            text_ += "友人・知人から　"
        }
        if(chekc4 == true){
            text_ += "セミナー　"
        }
        if(chekc5 == true){
            text_ += "その他"
        }
        
        text.text = text_
    }
}
