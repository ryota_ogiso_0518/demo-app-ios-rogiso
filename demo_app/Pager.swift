//
//  Pager.swift
//  demo_app
//
//  Created by rogiso on 2020/04/06.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit
class Pager: UIViewController,UIScrollViewDelegate{
   
   private var scrollView: UIScrollView!
  let cardWidth: CGFloat  = 300
        let cardHeight: CGFloat = 300
        var cardViews: [UIView] = []

        override func viewDidLoad() {
               super.viewDidLoad()
               
               // scrollViewの画面表示サイズを指定
               scrollView =  UIScrollView(frame: CGRect(x: 0, y: 200, width: self.view.frame.size.width, height: 300))
               // scrollViewのサイズを指定（幅は1メニューに表示するViewの幅×ページ数）
               scrollView.contentSize = CGSize(width: self.view.frame.size.width*3, height: 200)
               // scrollViewのデリゲートになる
               scrollView.delegate = self
               // メニュー単位のスクロールを可能にする
               scrollView.isPagingEnabled = true
               // 水平方向のスクロールインジケータを非表示にする
               scrollView.showsHorizontalScrollIndicator = false
               self.view.addSubview(scrollView)
               
               // scrollView上にUIImageViewをページ分追加する(今回は3ページ分)
               let imageView1 = createImageView(x: 0, y: 0, width: self.view.frame.size.width, height: 300, image: "Image")
               scrollView.addSubview(imageView1)
               
               let imageView2 = createImageView(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: 300, image: "Image-1")
               scrollView.addSubview(imageView2)
               
               let imageView3 = createImageView(x: self.view.frame.size.width*2, y: 0, width: self.view.frame.size.width, height: 300, image: "Image-2")
               scrollView.addSubview(imageView3)
    }
    func createImageView(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat, image: String) -> UIImageView {
        let imageView = UIImageView(frame: CGRect(x: x, y: y, width: width, height: height))
        let image = UIImage(named:  image)
        imageView.image = image
        return imageView
    }
}
