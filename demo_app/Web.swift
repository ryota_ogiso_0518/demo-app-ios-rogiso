//
//  Web.swift
//  demo_app
//
//  Created by rogiso on 2020/04/03.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit

class Web: UIViewController {

    @IBOutlet weak var web: UIWebView!
    
    let initialUrl = NSURL(string: "https://www.sonix.asia/")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.web.delegate = self as? UIWebViewDelegate
        let request = NSURLRequest(url: initialUrl! as URL)
        self.web.loadRequest(request as URLRequest)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
